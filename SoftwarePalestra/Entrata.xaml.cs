﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Windows.Threading;

namespace SoftwarePalestra
{
    /// <summary>
    /// Logica di interazione per Entrata.xaml
    /// </summary>
    public partial class Entrata : Window
    {
        public Entrata()
        {
            InitializeComponent();
            label_time.Content = "DATA ATTUALE";
            label_welcome.Content = "BENVENUTO";
            label_entrance.Content = "Entrate rimaste:";
            startclock();
        }

        private void startclock()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += tickevent;
            timer.Start();

        }

        private void tickevent(object sender, EventArgs e)
        {
            label_datetime.Content = DateTime.Now.ToString();
        }
        private void pictureBox_uk_Click(object sender, EventArgs e)
        {
            label_time.Content = "CURRENT DATE";
            label_welcome.Content = "WELCOME";
            label_entrance.Content = "Left entrance:";
        }
       
        private void pictureBox_italy_Click(object sender, EventArgs e)
        {
            label_time.Content = "DATA ATTUALE";
            label_welcome.Content = "BENVENUTO";
            label_entrance.Content = "Entrate rimaste:";
        }
    }
}
