﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SoftwarePalestra
{
    // aggiungo un commento di prova
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MSRController controller;

        private string[] currentUsr;

        private bool isModify;

        public MainWindow()
        {
            ClearFields();

            ModifyTxtBox(false);

            this.isModify = false;

            controller = new MSRController("COM3");
        }

        #region Button clicks

        private void btnModifica_Click(object sender, RoutedEventArgs e)
        {
            switch (isModify)
            {
                case true:
                    {
                        ModifyTxtBox(false);
                        this.isModify = false;
                        SaveModificatons();
                    }
                    break;

                case false:
                    {
                        ModifyTxtBox(true);
                        this.isModify = true;
                    }
                    break;
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearFields();
        }

        private void BtnScansiona_Click(object sender, RoutedEventArgs e)
        {
            currentUsr = controller.AdminReadFromCard();
            UpdateFields();
        }

        private void BtnNuovoUtente_Click(object sender, RoutedEventArgs e)
        {
            ClearFields();
            isModify = true;
            ModifyTxtBox(true);
        }

        #endregion

        #region modifiche campi

        protected void ClearFields()
        {
            InitializeComponent();
            txtName.Clear();
            txtEmail.Clear();
            txtNtelefono.Clear();
            txtCognome.Clear();
            txtCitta.Clear();
            txtIndirizzo.Clear();
            txtDataRegistrazione.Clear();
            txtDataNascita.Clear();
            txtLeftEntrance.Text = 0.ToString();
        }

        protected void UpdateFields()
        {
            try
            {
                DBQueries db = new DBQueries("localhost", "root", "", "mypalestra");
                txtName.Text = db.GetNome(int.Parse(currentUsr[0]));
                txtLeftEntrance.Text = currentUsr[1];

            }
            catch (IndexOutOfRangeException) { }
        }

        protected void SaveModificatons()
        {
            string messageBoxText = "Sei sicuro di effettuare le modifiche?";
            string caption = "Applicare le modifiche";
            MessageBoxButton button = MessageBoxButton.YesNo;
            MessageBoxImage icon = MessageBoxImage.Warning;
            MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
            switch (result)
            {
                case MessageBoxResult.Yes:
                    controller.AdminWriteToCard(txtName.Text, txtLeftEntrance.Text);
                    break;
                case MessageBoxResult.No:
                    ClearFields();
                    ModifyTxtBox(false);
                    break;
            }

        }

        protected void ModifyTxtBox(bool enabled)
        {
            txtName.IsEnabled = enabled;
            txtEmail.IsEnabled = enabled;
            txtNtelefono.IsEnabled = enabled;
            txtCognome.IsEnabled = enabled;
            txtCitta.IsEnabled = enabled;
            txtIndirizzo.IsEnabled = enabled;
            txtDataRegistrazione.IsEnabled = enabled;
            txtLeftEntrance.IsEnabled = enabled;
            txtDataNascita.IsEnabled = enabled;
            if (enabled)
            {
                btnModifica.Content = "Salva";
            }
            else
            {
                btnModifica.Content = "Modifica";
            }
        }


        #endregion

        private void BtnStampa_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Corsi_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
